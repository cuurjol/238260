# frozen_string_literal: true

require 'forwardable'
require 'uri'

class Url
  include Comparable
  extend Forwardable

  attr_reader :url

  def_delegators :url, :scheme, :host

  def initialize(url)
    @url = URI(url)
  end

  def query_params
    (url.query || '').split('&').reduce({}) do |hash, str|
      k, v = str.split('=')
      v.nil? ? hash : hash.merge({ k.to_sym => v })
    end
  end

  def query_param(key, value = nil)
    query_params[key] || value
  end

  def <=>(other)
    url.to_s <=> other.url.to_s
  end
end
