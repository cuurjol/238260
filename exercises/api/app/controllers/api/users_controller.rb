# frozen_string_literal: true

class Api::UsersController < Api::ApplicationController
  def index
    render_object(User.all)
  end

  def show
    render_object(find_user)
  end

  private

  def find_user
    User.find(params[:id])
  end

  def render_object(object)
    respond_with(object.as_json(except: %i[email password_digest]))
  end
end
