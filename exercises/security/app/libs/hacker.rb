# frozen_string_literal: true

require 'open-uri'
require 'net/http'

class Hacker
  class << self
    def hack(email, password)
      base_url = 'https://rails-l4-collective-blog.herokuapp.com'
      sign_up_uri = URI.parse("#{base_url}/users/sign_up")
      create_user_uri = URI.parse("#{base_url}/users")

      response = Net::HTTP.get_response(sign_up_uri)
      document = Nokogiri::HTML.parse(response.body)
      authenticity_token = document.at('input[name="authenticity_token"]')['value']

      headers = { cookie: response.header['set-cookie'].split('; ').first }
      params = {
        authenticity_token: authenticity_token,
        user: { email: email, password: password, password_confirmation: password }
      }

      Net::HTTP.post(create_user_uri, URI.encode_www_form(params), headers)
    end
  end
end
