# frozen_string_literal: true

class Article < ApplicationRecord
  def last_reading_date
    Rails.cache.fetch("#{cache_key_with_version}", expires_in: 12.hours) { Time.now }
  end
end
