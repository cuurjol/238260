json.extract!(user, :id, :email, :address)
json.set!(:full_name, user.full_name)
json.posts(user.posts) do |post|
  json.partial!('api/v1/users/posts', post: post)
end
