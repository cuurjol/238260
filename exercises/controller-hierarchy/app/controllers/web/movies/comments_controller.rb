# frozen_string_literal: true

module Web
  module Movies
    class CommentsController < ApplicationController
      def index
        @comments = resource_movie.comments.order(id: :desc)
      end

      def new
        @comment = Comment.new
      end

      def create
        @comment = resource_movie.comments.build(comment_params)

        if @comment.save
          redirect_to(movie_comments_path(resource_movie), notice: t('success'))
        else
          render(:new, notice: t('fail'))
        end
      end

      def edit
        @comment = find_comment
      end

      def update
        @comment = find_comment

        if @comment.update(comment_params)
          redirect_to(movie_comments_path(resource_movie), notice: t('success'))
        else
          render(:edit, notice: t('fail'))
        end
      end

      def destroy
        @comment = find_comment

        if @comment.destroy
          redirect_to(movie_comments_path(resource_movie), notice: t('success'))
        else
          redirect_to(movie_comments_path(resource_movie), notice: t('fail'))
        end
      end

      private

      def find_comment
        Comment.find(params[:id])
      end

      def comment_params
        params.require(:comment).permit(:body)
      end
    end
  end
end
