# frozen_string_literal: true

module Web
  module Movies
    class ReviewsController < ApplicationController
      def index
        @reviews = resource_movie.reviews.order(id: :desc)
      end

      def new
        @review = Review.new
      end

      def create
        @review = resource_movie.reviews.build(review_params)

        if @review.save
          redirect_to(movie_reviews_path(resource_movie), notice: t('success'))
        else
          render(:new, notice: t('fail'))
        end
      end

      def show
        @review = find_review
      end

      def edit
        @review = find_review
      end

      def update
        @review = find_review

        if @review.update(review_params)
          redirect_to(movie_reviews_path(resource_movie), notice: t('success'))
        else
          render(:edit, notice: t('fail'))
        end
      end

      def destroy
        @review = find_review

        if @review.destroy
          redirect_to(movie_reviews_path(resource_movie), notice: t('success'))
        else
          redirect_to(movie_reviews_path(resource_movie), notice: t('fail'))
        end
      end

      private

      def find_review
        Review.find(params[:id])
      end

      def review_params
        params.require(:review).permit(:body)
      end
    end
  end
end
