# frozen_string_literal: true

def anagramm_filter(word, array)
  array.select { |x| word.chars.sort == x.chars.sort }
end
