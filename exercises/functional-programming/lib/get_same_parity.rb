# frozen_string_literal: true

def get_same_parity(array)
  return [] if array.empty?

  array[0].even? ? array.select(&:even?) : array.select(&:odd?)
end
