# frozen_string_literal: true

def count_by_years(users)
  users.select { |h| h[:gender] == 'male' }.group_by { |h| h[:birthday][0..3] }.transform_values(&:size)
end
