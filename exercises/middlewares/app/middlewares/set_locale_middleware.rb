# frozen_string_literal: true

class SetLocaleMiddleware
  attr_reader :app

  def initialize(app)
    @app = app
  end

  def call(env)
    locale = extract_locale(env['HTTP_ACCEPT_LANGUAGE'])
    I18n.locale = I18n.available_locales.include?(locale) ? locale : I18n.default_locale

    print_logger_message

    status, headers, response = app.call(env)
    [status, headers, response]
  end

  private

  def print_logger_message
    Rails.logger.debug('*' * 25)
    Rails.logger.debug { "HTTP_ACCEPT_LANGUAGE: #{I18n.locale}" }
    Rails.logger.debug('*' * 25)
  end

  def extract_locale(header_locale)
    header_locale.scan(/^[a-z]{2}/).first.to_sym unless header_locale.nil?
  end
end
