# frozen_string_literal: true

class Vacancy < ApplicationRecord
  include AASM

  validates :title, :description, :publication_state, presence: true

  aasm column: :publication_state, whiny_transitions: false do
    state :on_moderate, initial: true
    state :published
    state :archived

    event :publish do
      transitions from: :on_moderate, to: :published
    end

    event :archive do
      transitions from: %i[on_moderate published], to: :archived
    end
  end
end
