# frozen_string_literal: true

class VacanciesController < ApplicationController
  before_action :set_vacancy, only: %i[publish archive]

  def index
    @on_moderate = Vacancy.on_moderate
    @published = Vacancy.published
  end

  def new
    @vacancy = Vacancy.new
  end

  def create
    @vacancy = Vacancy.new(vacancy_params)

    if @vacancy.save
      redirect_to(vacancies_path, notice: 'Vacancy was successfully created.')
    else
      render(:new, status: :unprocessable_entity)
    end
  end

  def publish
    if @vacancy.publish!
      redirect_to(vacancies_path, notice: 'Vacancy was successfully published.')
    else
      redirect_to(vacancies_path, notice: 'Vacancy was not successfully published.')
    end
  end

  def archive
    if @vacancy.archive!
      redirect_to(vacancies_path, notice: 'Vacancy was successfully archived.')
    else
      redirect_to(vacancies_path, notice: 'Vacancy was not successfully archived.')
    end
  end

  private

  def vacancy_params
    params.require(:vacancy).permit(:title, :description)
  end

  def set_vacancy
    @vacancy = Vacancy.find(params[:id])
  end
end
