# frozen_string_literal: true

Rails.application.routes.draw do
  root 'vacancies#index'

  resources :vacancies do
    member do
      patch :publish
      patch :archive
    end
  end
end
