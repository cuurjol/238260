# frozen_string_literal: true

require 'date'

module Model
  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
    attr_reader :scheme

    def attribute(name, **options)
      @scheme ||= {}
      @scheme[name] = options

      define_method(name) { @attributes[name] }
      define_method("#{name}=") { |value| @attributes[name] = convert_value(value, options[:type]) }
    end
  end

  attr_reader :attributes

  def initialize(attributes = {})
    @attributes = self.class.scheme.each_with_object({}) do |(name, options), result|
      result[name] = convert_value(attributes[name] || options[:default], options[:type])
    end
  end

  private

  def convert_value(value, type)
    return if value.nil?

    case type
    when :integer then value.to_i
    when :string then value.to_s
    when :boolean then value.to_s.downcase == 'true'
    when :datetime then DateTime.parse(value)
    else value
    end
  end
end
