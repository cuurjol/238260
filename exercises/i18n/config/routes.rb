# frozen_string_literal: true

Rails.application.routes.draw do
  scope '(:locale)', locale: /#{I18n.available_locales.join("|")}/ do
    root 'home#index'

    resources :posts do
      resources :comments, except: %i[index new show], module: :posts
    end
  end
end
