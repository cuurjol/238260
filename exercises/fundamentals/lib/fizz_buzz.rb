# frozen_string_literal: true

def fizz_buzz(start, stop)
  return if start > stop

  (start..stop).each_with_object([]) do |x, arr|
    arr << if (x % 15).zero?
             'FizzBuzz'
           elsif (x % 3).zero?
             'Fizz'
           elsif (x % 5).zero?
             'Buzz'
           else
             x
           end
  end.join(' ')
end
