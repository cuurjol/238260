# frozen_string_literal: true

# def fibonacci(num)
#   return if num <= 0

#   num <= 2 ? num - 1 : fibonacci(num - 1) + fibonacci(num - 2)
# end

def fibonacci(num)
  return if num <= 0

  func = lambda do |current, acc1, acc2|
    return acc1 if current == num

    func.call(current + 1, acc2, acc1 + acc2)
  end

  func.call(1, 0, 1)
end
