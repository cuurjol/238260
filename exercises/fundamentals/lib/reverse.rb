# frozen_string_literal: true

# def reverse(str)
#   str.size <= 1 ? str : str[-1] + reverse(str[0..-2])
# end

def reverse(str)
  func = lambda do |current, acc|
    return acc if acc.size == str.size

    func.call(current[0..-2], acc << current[-1])
  end

  func.call(str.chars[0..-2], str[-1])
end
