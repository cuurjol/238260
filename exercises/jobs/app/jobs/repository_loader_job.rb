class RepositoryLoaderJob < ApplicationJob
  queue_as :default

  def perform(id)
    repository = Repository.find(id)
    link = repository.link
    repository.start_fetching!

    if repository.update(updated_params(link))
      repository.finish_fetching!
    else
      repository.fail_fetching!
    end
  rescue Octokit::NotFound, Net::HTTPExceptions
    repository.fail_fetching!
  end

  private

  def updated_params(link)
    octokit_repo = Octokit::Repository.from_url(link)
    owner = octokit_repo.owner
    name = octokit_repo.name
    slug = octokit_repo.slug
    repository_info = Octokit.client.repo("#{slug}")

    {
      owner_name: owner,
      repo_name: name,
      description: repository_info.description,
      default_branch: repository_info.default_branch,
      watchers_count: repository_info.watchers_count,
      language: repository_info.language,
      repo_created_at: repository_info.created_at,
      repo_updated_at: repository_info.updated_at
    }
  end
end
