# frozen_string_literal: true

class Repository < ApplicationRecord
  include AASM

  validates :link, presence: true, uniqueness: true

  aasm whiny_transitions: false do
    state :created, initial: true
    state :fetching
    state :fetched
    state :failed

    event :start_fetching do
      transitions from: %i[created failed], to: :fetching
    end

    event :finish_fetching do
      transitions from: :fetching, to: :fetched
    end

    event :fail_fetching do
      transitions from: :fetching, to: :failed
    end
  end
end
