# frozen_string_literal: true

module Posts
  class CommentsController < ApplicationController
    before_action :set_post
    before_action :set_comment, only: %i[edit update destroy]

    def create
      @comment = @post.comments.build(comment_params)

      if @comment.save
        redirect_to @post, notice: 'Comment has been added'
      else
        flash[:alert] = 'Comment has not been added'
        render 'posts/show'
      end
    end

    def edit; end

    def update
      if @comment.update(comment_params)
        redirect_to @post, notice: 'Comment has been updated'
      else
        flash[:alert] = 'Comment has not been updated'
        render :edit
      end
    end

    def destroy
      if @comment.destroy
        flash[:notice] = 'Comment has been removed'
      else
        flash[:alert] = 'Comment has not been removed'
      end

      redirect_to @post
    end

    private

    def set_post
      @post = Post.find(params[:post_id])
    end

    def set_comment
      @comment = @post.comments.find(params[:id])
    end

    def comment_params
      params.require(:post_comment).permit(:body)
    end
  end
end
