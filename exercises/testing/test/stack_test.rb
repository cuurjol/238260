# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  def setup
    @elements = %w[cat dog mouse]
    @stack = Stack.new(@elements)
  end

  def test_create_stack_object_with_elements
    assert { @stack.to_a == @elements }
  end

  def test_create_stack_object_by_default
    @stack = Stack.new
    assert { @stack.instance_variable_get(:@elements) == [] }
    assert { @stack.instance_of?(Stack) }
  end

  def test_show_stack_elements
    assert { @stack.to_a == @elements }
  end

  def test_show_stack_size
    assert { @stack.size == 3 }
  end

  def test_clear_stack
    assert { @stack.to_a == @elements }
    @stack.clear!
    assert { @stack.to_a == [] }
  end

  def test_push_element_to_stack
    assert { @stack.to_a == @elements }
    @stack.push!('parrot')
    assert { @stack.to_a == %w[cat dog mouse parrot] }
  end

  def test_pop_element_from_stack
    assert { @stack.to_a == @elements }
    assert { @stack.pop! == 'mouse' }
    assert { @stack.to_a == %w[cat dog] }
  end

  def test_pop_empty_stack
    @stack = Stack.new
    assert { @stack.to_a == [] }
    assert { @stack.pop!.nil? }
    assert { @stack.to_a == [] }
  end

  def test_check_empty_stack
    assert { @stack.empty? != true }
    @stack.clear!
    assert { @stack.empty? == true }
  end
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
