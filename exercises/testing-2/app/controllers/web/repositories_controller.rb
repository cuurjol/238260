# frozen_string_literal: true

require 'octokit'

class Web::RepositoriesController < Web::ApplicationController
  def index
    @repositories = Repository.all
  end

  def new
    @repository = Repository.new
  end

  def show
    @repository = Repository.find params[:id]
  end

  def create
    @repository = Repository.new(permitted_params.merge(octokit_repository_params))

    if @repository.save
      redirect_to(repository_path(@repository), notice: t('success'))
    else
      render(:new, notice: t('fail'))
    end
  end

  def edit
    @repository = Repository.find params[:id]
  end

  def update
    @repository = Repository.find params[:id]

    if @repository.update(permitted_params)
      redirect_to repositories_path, notice: t('success')
    else
      render :edit, notice: t('fail')
    end
  end

  def destroy
    @repository = Repository.find params[:id]

    if @repository.destroy
      redirect_to repositories_path, notice: t('success')
    else
      redirect_to repositories_path, notice: t('fail')
    end
  end

  private

  def octokit_repository_params
    octokit_repo = Octokit::Repository.from_url(permitted_params[:link])
    owner = octokit_repo.owner
    name = octokit_repo.name
    slug = octokit_repo.slug
    repository_info = Octokit.client.repo("#{slug}")

    {
      owner_name: owner,
      repo_name: name,
      description: repository_info.description,
      default_branch: repository_info.default_branch,
      watchers_count: repository_info.watchers_count,
      language: repository_info.language,
      repo_created_at: repository_info.created_at,
      repo_updated_at: repository_info.updated_at
    }
  end

  def permitted_params
    params.require(:repository).permit(:link)
  end
end
