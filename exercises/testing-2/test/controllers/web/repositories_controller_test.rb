# frozen_string_literal: true

require 'test_helper'

class Web::RepositoriesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @test_response = load_fixture('files/response.json')
    @json_response = JSON.parse(@test_response)
    @url = @json_response['html_url']
    @api_url = 'https://api.github.com'
    @named_api_path = Octokit::Repository.from_url(@url).named_api_path
    @owner, @name = Octokit::Repository.from_url(@url).slug.split('/')
    @test_params = { repository: { link: @url } }
  end

  def test_should_create
    stub_request(:get, "#{@api_url}/#{@named_api_path}").to_return(headers: { 'Content-Type' => 'application/json' }, body: @test_response, status: 200)
    assert_difference('Repository.count') { post(repositories_url, params: @test_params) }

    repository = Repository.find_by(link: @url)
    assert_redirected_to repository_url(Repository.find_by(link: @url))
    assert { repository.owner_name == @owner }
    assert { repository.repo_name == @name }
    assert { repository.description == @json_response['description'] }
    assert { repository.default_branch == @json_response['default_branch'] }
    assert { repository.watchers_count == @json_response['watchers_count'] }
    assert { repository.language == @json_response['language'] }
    assert { repository.repo_created_at == @json_response['created_at'] }
    assert { repository.repo_updated_at == @json_response['updated_at'] }
  end
end
