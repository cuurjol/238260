# frozen_string_literal: true

require 'digest'

class Signature
  def initialize(app)
    @app = app
  end

  def call(env)
    status, headers, body = @app.call(env)

    if status == 200
      result = Digest::SHA256.hexdigest(body.first)
      [200, headers, body << result]
    else
      [status, headers, body]
    end
  end
end
