# frozen_string_literal: true

class AdminPolicy
  def initialize(app)
    @app = app
  end

  def call(env)
    verb = env['REQUEST_METHOD']
    path = env['PATH_INFO']

    if verb == 'GET' && path.match?(/^\/admin(\/{0,1}$|\/[[:alpha:]]+$)/)
      [403, {}, []]
    else
      @app.call(env)
    end
  end
end
