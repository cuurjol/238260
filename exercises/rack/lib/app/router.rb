# frozen_string_literal: true

require 'rack'

class Router
  def call(env)
    verb = env['REQUEST_METHOD']
    path = env['PATH_INFO']

    if verb == 'GET' && path == '/'
      [200, {'Content-Type' => 'text/plain'}, ['Hello, World!']]
    elsif verb == 'GET' && path == '/about'
      [200, {'Content-Type' => 'text/plain'}, ['About page']]
    else
      [404, {'Content-Type' => 'text/plain'}, ['404 Not Found']]
    end
  end
end
