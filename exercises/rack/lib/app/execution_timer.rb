# frozen_string_literal: true

class ExecutionTimer
  def initialize(app)
    @app = app
  end

  def call(env)
    before = Time.now
    status, headers, body = @app.call(env)
    headers['X-Timing'] = (Time.now - before).to_s

    [status, headers, body]
  end
end
