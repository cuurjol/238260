statuses = %w[new in_progress done].map { |name| Status.create(name: name) }
users = 3.times.map { User.create(name: Faker::Name.name_with_middle) }

15.times do
  params = { name: Faker::Lorem.sentence, description: Faker::Lorem.paragraph_by_chars,
             user: users.sample, status: statuses.sample }
  Task.create(params)
end
