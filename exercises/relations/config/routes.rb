# frozen_string_literal: true

Rails.application.routes.draw do
  root 'home#index'

  resources :tasks
  resources :statuses
  resources :users
end
