# frozen_string_literal: true

class Status < ApplicationRecord
  has_many :tasks, dependent: :destroy

  validates :name, inclusion: { in: %w[new in_progress done] }
end
