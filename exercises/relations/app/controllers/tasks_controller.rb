# frozen_string_literal: true

class TasksController < ApplicationController
  def index
    render(:index, locals: { tasks: Task.all })
  end

  def new
    initialize_task { |task| render(:new, locals: { task: task }) }
  end

  def create
    initialize_task do |task|
      if task.save
        redirect_to(task, notice: 'Task was successfully created.')
      else
        render(:new, locals: { task: task })
      end
    end
  end

  def show
    find_task { |task| render(:show, locals: { task: task }) }
  end

  def edit
    find_task { |task| render(:edit, locals: { task: task }) }
  end

  def update
    find_task do |task|
      if task.update(task_params)
        redirect_to(task, notice: 'Task was successfully updated.')
      else
        render(:edit, locals: { task: task })
      end
    end
  end

  def destroy
    find_task do |task|
      task.destroy
      redirect_to(tasks_path, notice: 'Task was successfully destroyed.')
    end
  end

  private

  def initialize_task
    task = Task.new(task_params)
    yield(task)
  end

  def find_task
    task = Task.find(params[:id])
    yield(task)
  end

  def task_params
    params.fetch(:task, {}).permit(:name, :description, :user_id, :status_id)
  end
end
