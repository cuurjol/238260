# frozen_string_literal: true

class StatusesController < ApplicationController
  def index
    render(:index, locals: { statuses: Status.all })
  end

  def new
    initialize_status { |status| render(:new, locals: { status: status }) }
  end

  def create
    initialize_status do |status|
      if status.save
        redirect_to(status, notice: 'Status was successfully created.')
      else
        render(:new, locals: { status: status })
      end
    end
  end

  def show
    find_status { |status| render(:show, locals: { status: status }) }
  end

  def edit
    find_status { |status| render(:edit, locals: { status: status }) }
  end

  def update
    find_status do |status|
      if status.update(status_params)
        redirect_to(status, notice: 'Status was successfully updated.')
      else
        render(:edit, locals: { status: status })
      end
    end
  end

  def destroy
    find_status do |status|
      status.destroy
      redirect_to(statuses_path, notice: 'Status was successfully destroyed.')
    end
  end

  private

  def initialize_status
    status = Status.new(status_params)
    yield(status)
  end

  def find_status
    status = Status.find(params[:id])
    yield(status)
  end

  def status_params
    params.fetch(:status, {}).permit(:name)
  end
end
