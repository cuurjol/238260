# frozen_string_literal: true

class UsersController < ApplicationController
  def index
    render(:index, locals: { users: User.all })
  end

  def new
    initialize_user { |user| render(:new, locals: { user: user }) }
  end

  def create
    initialize_user do |user|
      if user.save
        redirect_to(user, notice: 'User was successfully created.')
      else
        render(:new, locals: { user: user })
      end
    end
  end

  def show
    find_user { |user| render(:show, locals: { user: user }) }
  end

  def edit
    find_user { |user| render(:edit, locals: { user: user }) }
  end

  def update
    find_user do |user|
      if user.update(user_params)
        redirect_to(user, notice: 'User was successfully updated.')
      else
        render(:edit, locals: { user: user })
      end
    end
  end

  def destroy
    find_user do |user|
      user.destroy
      redirect_to(users_path, notice: 'User was successfully destroyed.')
    end
  end

  private

  def initialize_user
    user = User.new(user_params)
    yield(user)
  end

  def find_user
    user = User.find(params[:id])
    yield(user)
  end

  def user_params
    params.fetch(:user, {}).permit(:name)
  end
end
