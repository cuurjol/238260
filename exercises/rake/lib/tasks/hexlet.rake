# frozen_string_literal: true

require 'csv'

namespace :hexlet do
  desc 'Create users from input file'
  task :import_users, [:filename] => :environment do |_t, args|
    CSV.foreach(args[:filename], headers: true) do |row|
      month, day, year = row['birthday'].split('/')
      row['birthday'] = [year, month, day].join('-')
      User.create!(row.to_h)
    end
  end
end
