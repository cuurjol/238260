require 'test_helper'

class TasksControllerTest < ActionController::TestCase
  setup do
    @task = tasks(:task_1)
    @task_params = { name: 'Name 999', creator: 'Creator 999', completed: false }
  end

  test 'it shows all tasks' do
    get(:index)

    assert_response(200)
    assert_equal(10, Task.count)
  end

  test 'it shows a new form page' do
    get(:new)

    assert_response(200)
    assert_select('h1', 'New Task')
  end

  test 'it creates a new task' do
    assert_difference('Task.count') { post(:create, params: { task: @task_params }) }

    assert_response(302)
    assert_redirected_to(task_path(Task.last))
    assert_equal('Task was successfully created.', flash[:notice])
  end

  test 'it shows one task' do
    get(:show, params: { id: @task.id })

    assert_response(200)
    assert_includes(@response.body, @task.name)
    assert_includes(@response.body, @task.creator)
    assert_includes(@response.body, @task.status)
  end

  test 'it shows an edit form page' do
    get(:edit, params: { id: @task.id })

    assert_response(200)
    assert_select('h1', 'Edit Task')
  end

  test 'it updates an existing task' do
    assert_no_difference('Task.count') { patch(:update, params: { id: @task.id, task: @task_params }) }

    assert_response(302)
    assert_redirected_to(task_path(@task))
    assert_equal('Task was successfully updated.', flash[:notice])

    @task.reload
    assert_equal(@task_params[:name], @task.name)
    assert_equal(@task_params[:creator], @task.creator)
    assert_equal(@task_params[:completed], @task.completed)
  end

  test 'it destroys an existing task' do
    assert_difference('Task.count', -1) { delete(:destroy, params: { id: @task.id }) }

    assert_response(302)
    assert_redirected_to(tasks_path)
    assert_equal('Task was successfully destroyed.', flash[:notice])
  end
end
