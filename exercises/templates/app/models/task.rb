# frozen_string_literal: true

class Task < ApplicationRecord
  validates :name, :creator, presence: true
  validates :status, inclusion: { in: %w[new started testing finished] }
  validates :completed, inclusion: { in: [true, false] }
end
