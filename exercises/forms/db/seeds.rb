10.times do
  params = { title: Faker::Movies::VForVendetta.character, body: Faker::Movies::VForVendetta.speech,
             published: Faker::Boolean.boolean, summary: Faker::Movies::VForVendetta.quote }
  Post.create(params)
end
