class PostsController < ApplicationController
  def index
    render(:index, locals: { posts: Post.all })
  end

  def new
    initialize_post { |post| render(:new, locals: { post: post }) }
  end

  def create
    initialize_post do |post|
      if post.save
        redirect_to(post, notice: 'Post was successfully created.')
      else
        render(:new, locals: { post: post })
      end
    end
  end

  def show
    find_post { |post| render(:show, locals: { post: post }) }
  end

  def edit
    find_post { |post| render(:edit, locals: { post: post }) }
  end

  def update
    find_post do |post|
      if post.update(post_params)
        redirect_to(post, notice: 'Post was successfully updated.')
      else
        render(:edit, locals: { post: post })
      end
    end
  end

  def destroy
    find_post do |post|
      post.destroy
      redirect_to(posts_path, notice: 'Post was successfully destroyed.')
    end
  end

  private

  def initialize_post
    post = Post.new(post_params)
    yield(post)
  end

  def find_post
    post = Post.find(params[:id])
    yield(post)
  end

  def post_params
    params.fetch(:post, {}).permit(:title, :body, :summary, :published)
  end
end
