# frozen_string_literal: true

Rails.application.routes.draw do
  mount RailsStats::Engine, at: '/stats'
  mount Blog::Engine, at: '/blog'

  scope module: :web do
    root 'home#index'
  end
end
