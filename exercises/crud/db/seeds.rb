statuses = %w[new started testing finished]
booleans = [true, false]

10.times do
  params = { name: Faker::Lorem.sentence, description: Faker::Lorem.paragraph_by_chars,
             status: statuses.sample, creator: Faker::Name.name_with_middle,
             performer: Faker::Name.name_with_middle, completed: booleans.sample }
  Task.create(params)
end
