class TasksController < ApplicationController
  def index
    @tasks = Task.all
  end

  def new
    @task = Task.new
  end

  def create
    @task = Task.new(task_params)

    if @task.save
      redirect_to(@task, notice: 'Task was successfully created.')
    else
      render(:new)
    end
  end

  def show
    @task = find_task
  end

  def edit
    @task = find_task
  end

  def update
    @task = find_task

    if @task.update(task_params)
      redirect_to(@task, notice: 'Task was successfully updated.')
    else
      render(:edit)
    end
  end

  def destroy
    @task = find_task

    if @task.destroy
      redirect_to(tasks_path, notice: 'Task was successfully destroyed.')
    else
      redirect_to(task_path(@task), alert: 'Task was not successfully destroyed.')
    end
  end

  private

  def find_task
    Task.find(params[:id])
  end

  def task_params
    params.require(:task).permit(:name, :description, :status, :creator, :performer, :completed)
  end
end
