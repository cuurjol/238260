# frozen_string_literal: true

class Task < ApplicationRecord
  validates :name, :creator, presence: true
  validates :status, inclusion: { in: %w[new started testing finished] }
end
