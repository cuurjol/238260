# frozen_string_literal: true

require 'application_system_test_case'

class PostsTest < ApplicationSystemTestCase
  setup do
    @post = posts(:one)
  end

  test 'visiting a list of posts' do
    visit(posts_url)

    assert_selector('h1.h2', text: 'Posts')
    assert_selector('a.btn.btn-primary', text: 'New Post')
    assert_selector(:xpath, './/tbody/tr', count: Post.count)
  end

  test 'creating a post' do
    visit(posts_url)
    click_link('New Post')

    fill_in('Title', with: 'Hello World!')
    fill_in('Body', with: 'Test body post!')
    click_button('Create Post')

    assert_text('Post was successfully created.')
    assert_selector('h1.h2', text: 'Hello World!')
    assert_selector('p', text: 'Test body post!')
  end

  test 'creating a comment for existing post' do
    visit(post_url(@post))

    fill_in('post_comment_body', with: 'Test comment for existing post')
    click_button('Create Comment')

    assert_text('Comment was successfully created.')
    assert_selector('h3', text: 'Comments')
    assert_selector('small.font-weight-bold', text: 'Test comment for existing post')
  end

  test 'updating a post' do
    visit(posts_url)
    find_link(href: edit_post_path(@post)).click

    fill_in('Title', with: 'Hello World!')
    fill_in('Body', with: 'Test body post!')
    click_button('Update Post')

    assert_text('Post was successfully updated.')
    assert_selector('h1.h2', text: 'Hello World!')
    assert_selector('p', text: 'Test body post!')
  end

  test 'destroying a post' do
    visit(posts_url)
    page.accept_confirm { find_link(text: 'Destroy', href: post_path(@post)).click }

    assert_text('Post was successfully destroyed.')
    assert_selector('h1.h2', text: 'Posts')
    assert_selector('a.btn.btn-primary', text: 'New Post')
    assert_selector(:xpath, './/tbody/tr', count: Post.count)
  end
end
