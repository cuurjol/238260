# frozen_string_literal: true

require 'test_helper'

class BulletinsControllerTest < ActionDispatch::IntegrationTest
  test 'show all bulletins' do
    get(root_url)

    assert_response(:success)
    assert_equal(2, Bulletin.count)
  end

  test 'show one bulletin' do
    published = bulletins(:published)
    get(bulletin_url(published))

    assert_response(:success)
    assert_select('h3', published.title)
    assert_select('p', published.body)
  end
end
