# frozen_string_literal: true

class PostPolicy < ApplicationPolicy
  def create?
    user.present?
  end

  def update?
    user.present? && (record.author == user || user.admin?)
  end

  def destroy?
    user.present? && user.admin?
  end
end
