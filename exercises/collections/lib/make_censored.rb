# frozen_string_literal: true

def make_censored(text, stop_words)
  text.split.map { |x| stop_words.include?(x) ? '$#%!' : x }.join(' ')
end
