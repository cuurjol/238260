# frozen_string_literal: true

def compare_versions(version1, version2)
  version1.split('.').map(&:to_i) <=> version2.split('.').map(&:to_i)
end
