# frozen_string_literal: true

def build_query_string(params)
  params.keys.sort.map { |k| "#{k}=#{params[k]}" }.join('&')
end
